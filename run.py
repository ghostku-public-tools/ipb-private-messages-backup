from robobrowser import RoboBrowser
from datetime import datetime, timedelta
from time import sleep
import csv
import re
import click


def convert_date(date_str: str) -> datetime:
    """Convert DateTime string to datetime object."""
    date, time = date_str.split(", ")
    if date == "Вчора":
        date = (datetime.today() - timedelta(days=1)).strftime("%b %d %Y")
    date_time = f"{date} {time}"
    date_time = datetime.strptime(date_time, "%b %d %Y %H:%M")
    return date_time


class BackupMessages0Day:
    """A tool to download Private messages from IPB forums."""

    LOGIN_URL = "https://forum.0day.kiev.ua/index.php?act=Login"
    MESSAGES_URL = "https://forum.0day.kiev.ua/index.php?act=Msg&CODE=1&VID={folder}&sort=&st={start_from}"
    MESSAGE_URL = (
        "https://forum.0day.kiev.ua/index.php?act=Msg&CODE=03&VID=in&MSID={id}"
    )

    FOLDERS = ["in", "sent"]

    def __init__(self, log: str, pwd: str) -> None:
        self.log = log
        self.pwd = pwd
        self.browser = RoboBrowser(history=True)

    def login(self) -> None:
        self.browser.open(self.LOGIN_URL)
        # TODO: Переписать на поиск формы с нужными параметрам потому как сейчас мы
        # просто надеемся что на странице будет единственная форма
        form = self.browser.get_form()
        form["UserName"].value = self.log
        form["PassWord"].value = self.pwd
        self.browser.submit_form(form)

    def get_all_messages_meta(self) -> list:
        messages = []
        for folder in self.FOLDERS:
            start_from = 0
            while True:
                url = self.MESSAGES_URL.format(start_from=start_from, folder=folder)
                self.browser.open(url)
                message_rows = self.browser.find_all("tr", {"id": True})
                if not message_rows:
                    break
                messages += [
                    {
                        "id": int(row["id"]),
                        "user_name": row.select("td a")[1].text,
                        "user_url": row.select("td a")[1]["href"],
                        "direction": "Out" if folder == "sent" else "In",
                        "subject": row.select("td a")[0].text,
                        "date": convert_date(row.select("td")[3].text),
                    }
                    for row in message_rows
                ]
                start_from += 50
        return messages

    def get_message_text(self, message: dict) -> dict:
        REMOVE_STRINGS = [
            "<!--quoteo-->",
            '<div class="quotetop"></div>',
            "<!--QuoteEEnd-->",
            "<!--quotec-->",
            "<!--QuoteEnd-->",
        ]
        url = self.MESSAGE_URL.format(id=message["id"])
        self.browser.open(url)
        message_tag = self.browser.find("table", {"cellspacing": 1})
        text = str(message_tag.select(".post1 .postcolor").pop())
        text = re.match('<div class="postcolor">(.*)</div>', text).group(1)
        for remove_string in REMOVE_STRINGS:
            text = text.replace(remove_string, "")
        text = re.sub(r"<!--IBF.ATTACHMENT_\d*-->", "", text)
        text = text.replace("<br/>", "\n")
        message["text"] = text
        return message

    def get_all_messages(self) -> None:
        messages_meta = self.get_all_messages_meta()
        messages = []
        for message_meta in messages_meta:
            sleep(1)
            try:
                messages.append(self.get_message_text(message_meta))
            except AttributeError:
                sleep(10)
                messages.append(self.get_message_text(message_meta))
        return messages


@click.command()
@click.argument("log")
@click.argument("pwd")
@click.argument("output")
def backup(log: str, pwd: str, output: str) -> None:
    """Backup Private messages to CSV file."""
    test = BackupMessages0Day(log, pwd)
    test.login()
    messages = test.get_all_messages()
    with open(output, "w", encoding="utf-8-sig", newline="\n") as f:
        writer = csv.DictWriter(
            f, dialect="excel-tab", delimiter=";", fieldnames=messages[0].keys()
        )
        writer.writeheader()
        writer.writerows(messages)


if __name__ == "__main__":
    backup()
